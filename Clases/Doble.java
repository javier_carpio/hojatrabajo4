/** Doble.java
 * Lista de implementación doble encadenada.
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */

/**
 *
 * @author javie
 */
public class Doble<E> extends AbstractList<E>{
    //Atributis
    protected int count; //tamaño
    protected NodoDoble<E> head; //primer elemento
    protected NodoDoble<E> tail; //último elemento

    /**
     * Constructor
     */
    public Doble()
    // post: constructs an empty list
    {
       head = null;
       tail = null;
       count = 0;
    }

    /**
     * Anade el objeto E al final de la lista
     * @param value valor a anadir
     */
    @Override
    public void addLast(E value)
    // pre: value is not null
    // post: adds new value to tail of list
    {
        // construct new element
        tail = new NodoDoble<>(value, null, tail);
        // fix up head
        if (head == null) head = tail;
        count++;
    }
    
    /**
     * Remueve el ultimo elemento de la lista
     * @return el valor eliminado
     */
    @Override
    public E removeLast() 
    // pre: !isEmpty()
    // post: returns and removes value from tail of list
    {
        NodoDoble<E> temp = tail;
        tail = tail.previous();
        if (tail == null) {
            head = null;
        } else {
            tail.setNext(null);
        }
        count--;
        return temp.value();
    }
    
    /**
     * Indica el tamaño del nodo
     * @return count el tamaño
     */
    @Override
    public int size()  
    // post: returns size of list
    {
        return count;
    }
    
    /**
     * Pone el tamaño como 0
     * @return 0
     */
    @Override
    public boolean empty()
    // post: returns true if list is empty
    {
        return size() == 0;
    }

    /**
     * Agrega el elemento a la lista
     * @param item elemento a agregar
     */
    @Override
    public void push(E item)
    // pre: value non-null
    // post: adds element to tail of list
            {
        addLast(item);
    }

    /**
     * Elimina el último elemento
     * @return método para eliminar el último elemento
     */
    @Override
    public E pop()
    // pre: !isEmpty()
    // post: returns and removes value from tail of list
    {
        return removeLast();
    }

    /**
     * Muestra el último elemento agregado a la lista
     * @return último elemento
     */
    @Override
    public E peek()
    // pre: !isEmpty()
    // post: returns value from tail of list
    {
        return getLast();
    }

    /**
     * Muestra el último elemento en la lista
     * @return valor del último elemento
     */
    @Override
    public E getLast() 
    // pre: !isEmpty()
    // post: returns value from tail of list
    {
        return tail.value();
    }
}
