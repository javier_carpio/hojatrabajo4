/** Stack.java
 * Interfaz para la implementación del Stack
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */

/**
 *
 * @author javie
 */
public interface Stack<E> {
    /**
    * Metodo que permite agregar un objeto al stack.
    * @param item Objeto a guardarse en el stack.
    */
   public void push(E item);
   // post: item is added to stack
   
   /**
    * Metodo que permite tomar el primer objeto en el stack.
    * @return Ultimo objeto ingresado en el stack.
    */
   public E pop();
   // pre: stack is not empty
   // post: most recently pushed item is removed and returned
   
   /**
    * Metodo que nos permite observar el primero objeto en el stack.
    * @return ultimo valor
    */
   public E peek();
   // pre: stack is not empty
   // post: top value (next to be popped) is returned
   
   /**
    * Metodo que nos permite saber si esta vacio el stack.
    * @return True si el stack esta vacio, de lo contrario, false.
    */
   public boolean empty();
   // post: returns true if and only if the stack is empty
   
   /**
    * Metodo que nos permite conocer el tamano del stack
    * @return Numero de elementos en el stack.
    */
   public int size();
    // post: returns the number of elements in the stack
}
