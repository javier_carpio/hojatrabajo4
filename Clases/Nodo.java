/** Nodo.java
 * Dirige a la cabeza o cola
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */


/**
 *
 * @author javie
 */
public class Nodo<E> {
    protected E data; // value stored in this element
    protected Nodo<E> nextElement; // ref to next

    /**
     * Constructor
     * @param v valor
     * @param next cabeza
     */
    public Nodo(E v, Nodo<E> next)
    // pre: v is a value, next is a reference to 
    //      remainder of list
    // post: an element is constructed as the new 
    //      head of list
    {
        data = v;
        nextElement = next;
    }

    /**
     * Construye un solo elemento
     * @param v valor
     */
    public Nodo(E v)
    // post: constructs a new tail of a list with value v
    {
       this(v, null);
    }

    /**
     * Busca el siguiente valor en la lista
     * @return siguiente elemento
     */
    public Nodo<E> next()
    // post: returns reference to next value in list
    {
       return nextElement;
    }

    /**
     * Coloca la siguiente referencia
     * @param next valor
     */
    public void setNext(Nodo<E> next)
    // post: sets reference to new next value
    {
       nextElement = next;
    }
    
    /**
     * Retorna un valor asociado con el elemento
     * @return valor de E
     */
    public E value()
    // post: returns value associated with this element
    {
       return data;
    }

    /**
     * Ingresa un valor nuevo
     * @param value valor
     */
    public void setValue(E value)
    // post: sets value associated with this element
    {
       data = value;
    }
}
