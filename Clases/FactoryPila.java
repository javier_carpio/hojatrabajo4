/** FactoryPila.java
 * Factory de las pilas
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */


import java.util.Scanner;


public class FactoryPila {
    /**
     * Selecciona la lista escogida
     * @param opcion numero
     * @param texto letra
     * @return null
     */
    public Stack getLista(int opcion, String texto){
        String opcion2 = "";
        Scanner scaner = new Scanner(System.in);
        
        switch (opcion) {
            case 1:
                System.out.println("Esta utilizando ArrayList.");
                return new StackArrayList();
            case 2:
                System.out.println("Esta utilizando Vector.");
                return new StackVector();
            case 3:
                System.out.println("Seleccione otra opcion");
                System.out.println("A. Lista Simple.");
                System.out.println("B. Lista Doble.");
                System.out.println("C. Lista Circular.");
                opcion2 = scaner.next();
                opcion2 = opcion2.toLowerCase();
                while((!opcion2.equals("a")) && (!opcion2.equals("b")) && (!opcion2.equals("c"))){
                    System.out.println("Elija una opcion correcta.");
                    opcion2 = scaner.next();
                }
                
                switch (opcion2) {
                    case "a":
                        System.out.println("Esta utilizando listas simples.");
                        return new Simple();
                    case "b":
                        System.out.println("Esta utilizando listas dobles.");
                        return new Doble();
                    case "c":
                        System.out.println("Esta utilizando listas circulares.");
                        return new Circular();
                    default:
                        
                        break;
                }   break;
            default:
                return null;
        }
        
        return null;
    }
    
}
