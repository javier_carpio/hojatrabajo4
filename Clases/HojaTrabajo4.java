/** HojaTrabajo4.java
 * Principal e ingreso de datos.
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author javie
 */
public class HojaTrabajo4 {
    
    /**
     * 
     * @param args arguments
     * @throws FileNotFoundException if file is not found
     */
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scaner = new Scanner(System.in);
        FactoryPila factory = new FactoryPila();
        Stack stack;
        Calculadora calculadora1;
        
        File archivo = new File("datos.txt");
        String texto = new String();
        BufferedReader entrada;
        entrada = new BufferedReader(new FileReader(archivo));
        
        try{
            String linea = "";
            while(entrada.ready()){
                linea = entrada.readLine();
                texto += linea;
            }
            System.out.println(linea);
        
        }catch (IOException e) {
            
        }finally{
            try{
                entrada.close();
            }
            catch(IOException e1){
            }
        }
    
        boolean option = true;
        for (int i = 0; i < texto.length(); i++){
            if ((texto.codePointAt(i) != 32) && (texto.codePointAt(i) != 255) && 
                    (texto.codePointAt(i) != 42) && (texto.codePointAt(i) != 43) && 
                    (texto.codePointAt(i) != 45) && (texto.codePointAt(i) != 47) && 
                    (texto.codePointAt(i) != 48)  && (texto.codePointAt(i) != 49) && 
                    (texto.codePointAt(i) != 50) && (texto.codePointAt(i) != 51) && 
                    (texto.codePointAt(i) != 52) && (texto.codePointAt(i) != 53) && 
                    (texto.codePointAt(i) != 54) && (texto.codePointAt(i) != 55) && 
                    (texto.codePointAt(i) != 56) && (texto.codePointAt(i) != 57)){
                option = false;
                break;
            }else{
                option = true;
            }
        }
        if (option == false) {
            System.out.println("Revisar el txt, porque existe algo que no es operando o numero.");
        }
        if (option == true) {
            
            int op = 0;
            
            System.out.println("Elija una opcion:");
            System.out.println("1. ArrayList.");
            System.out.println("2. Vector.");
            System.out.println("3. Listas.");
            
            while(!scaner.hasNextInt()){
                System.out.println("Ingrese un numero valido, no una letra.");
                scaner.next();
            }
            op = scaner.nextInt();
            
            stack = factory.getLista(op, texto);
            calculadora1 = Calculadora.getInstance(stack);
            
            double resultado = calculadora1.operar(texto);
            
            if(Double.isNaN(resultado) || (Double.isInfinite(resultado))){
               System.out.println("Revisar el txt, porque ocurre una division entre 0.");
            }else{
               System.out.println("El resultado es:\t" + resultado + ".");
            }
        }
    }
}
