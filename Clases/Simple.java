/** Simple.java
 * Lista de implementación simple encadenada.
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */


import java.util.Iterator;

/**
 *
 * @author javie
 */
public class Simple<E> extends AbstractList<E>{
    //Atributos
    protected int count; // list size
   protected Nodo<E> head; // ref. to first element

   /**
    * Constructor
    */
   public Simple()
   // post: generates an empty list
   {
      head = null;
      count = 0;
   }
    
    /**
     * Indica el tamaño del nodo
     * @return count el tamaño
     */
    @Override
    public int size() 
    // post: returns the number of elements in the stack
    {
        return count;
    }

   /**
    * Añade un elemento al último lugar de la lista
    * @param value elemento a añadir
    */
    @Override
    public void addLast(E value)
    // post: the value is added to the tail of the stack
    {
        head = new Nodo<>(value, head);
        count ++;
    }

    /**
     * Muestra el último elemento en la lista, en este caso es el primero
     * @return valor del último elemento
     */
    @Override
    public E getLast() 
    // pre: stack is not empty
    // post: last value of tail is returned
    {
        //int contador = 0;
        
        return head.value();
    }

    /**
     * Remueve el ultimo elemento de la lista, en este caso siendo el primero
     * @return el valor eliminado
     */    
    @Override
    public E removeLast()
    // pre: stack is not empty
    // post: last value of tail is returned and removed
    {
        Nodo<E> temp = head;
        if(head.next()!=null){
            head=head.next();
        }else{
            head=null;
        }
        count--;
        return temp.value();
    }

     



}
