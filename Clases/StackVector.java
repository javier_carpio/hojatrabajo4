/** StackVector.java
 * Implementación de Pila tipo Vector
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */


import java.util.Vector;

/**
 *
 * @author javie
 */
public class StackVector<E> implements Stack<E>{
    //Atributos
    protected Vector<E> vector;
    
    /**
     * Constructor
     */
    public StackVector()
    // post: constructs a new, empty stack
    {
        vector = new Vector<>();
    }

    /**
     * Agrega el elemento a la lista
     * @param item elemento a agregar
     */
    @Override
    public void push(E item)
    // post: the value is added to the stack
    {
        vector.addElement(item);
    }

    /**
     * Elimina el último elemento
     * @return método para eliminar el último elemento
     */
    @Override
    public E pop() 
    // pre: stack is not empty
    // post: most recently pushed item is removed and returned
    {
        return vector.remove(size() - 1);
    }

    /**
     * Muestra el último elemento agregado a la lista
     * @return último elemento
     */
    @Override
    public E peek()
    // pre: stack is not empty
    // post: top value (next to be popped) is returned
    {
        return vector.get(size() - 1);
    }

    /**
     * Pone el tamaño como 0
     * @return está en 0
     */
    @Override
    public boolean empty()
    // post: returns true if list is empty
    {
        return size() == 0;
    }

    /**
     * Indica el tamaño del nodo
     * @return count el tamaño
     */
    @Override
    public int size() 
    // post: returns size of list
    {
        return vector.size();
    }
    
    
}
