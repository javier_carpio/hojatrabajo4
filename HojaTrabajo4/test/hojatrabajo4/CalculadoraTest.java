

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cooli
 */
public class CalculadoraTest {
    Stack stack;
    FactoryPila fact;
    Calculadora calcu;
    
    public CalculadoraTest() {
        fact = new FactoryPila();
        stack = fact.getLista(1 , "4 3 + 6 5 * - 2 /");
        calcu = Calculadora.getInstance(stack);
    }

    /**
     * Test of getInstance method, of class Calculadora.
     */
    @Test
    public void testGetInstance() {
        
        
    }

    /**
     * Test of operar method, of class Calculadora.
     */
    @Test
    public void testOperar() {
        double actual = calcu.operar("1 1 + 3 *");
        double expected=6;
        assertEquals(expected, actual, 0);
    }
    
}
