/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cooli
 */
public class StackVectorTest {
    StackVector test;
    
    public StackVectorTest() {
        test=new StackVector();
    }

    /**
     * Test of push method, of class StackVector.
     */
    @Test
    public void testPush() {
        test.push("5");
        boolean expected = test.empty();
        boolean real=false;
        assertEquals(expected,real);
    }

    /**
     * Test of pop method, of class StackVector.
     */
    @Test
    public void testPop() {
        test.push("5");
        test.pop();
        boolean expected = test.empty();
        boolean real = true;
        assertEquals(expected,real);
    }

    /**
     * Test of peek method, of class StackVector.
     */
    @Test
    public void testPeek() {
        test.push("5");
        Object real=test.peek();
        Object expected = "5";
        assertEquals(expected,real);
    }

    /**
     * Test of empty method, of class StackVector.
     */
    @Test
    public void testEmpty() {
        boolean real = test.empty();
        boolean exp = true;
        assertEquals(exp,real);
    }

    /**
     * Test of size method, of class StackVector.
     */
    @Test
    public void testSize() {
        int real=test.size();
        int exp=0;
        assertEquals(exp,real);
    }
    
}
