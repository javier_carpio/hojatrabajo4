/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cooli
 */
public class CircularTest {
    
    Circular test;
    
    public CircularTest() {
        test=new Circular();
    }

    /**
     * Test of addFirst method, of class Circular.
     */
    @Test
    public void testAddFirst() {
        test.addFirst("E");
        test.addFirst("F");
        Object real = test.getLast();
        Object expected = "E";
        assertEquals(expected,real);
    }

    /**
     * Test of addLast method, of class Circular.
     */
    @Test
    public void testAddLast() {
        test.addLast("E");
        test.addLast("F");
        Object real = test.getLast();
        Object expected = "F";
        assertEquals(expected,real);
    }

    /**
     * Test of removeLast method, of class Circular.
     */
    @Test
    public void testRemoveLast() {
        test.addLast("E");
        test.addLast("F");
        test.removeLast();
        Object real = test.getLast();
        Object expected = "E";
        assertEquals(expected,real);
    }

    /**
     * Test of push method, of class Circular.
     */
    @Test
    public void testPush() {
        test.push("A");
        boolean real = test.isEmpty();
        boolean expected = false;
        assertEquals(expected, real);
    }

    /**
     * Test of pop method, of class Circular.
     */
    @Test
    public void testPop() {
        test.push("A");
        test.pop();
        boolean real = test.isEmpty();
        boolean expected = true;
        assertEquals(expected, real);
    }

    /**
     * Test of peek method, of class Circular.
     */
    @Test
    public void testPeek() {
        test.push("A");
        Object real = test.peek();
        Object expected = "A";
        assertEquals(expected, real);
    }

    /**
     * Test of size method, of class Circular.
     */
    @Test
    public void testSize() {
        int expected = 0;
        int real = test.size();
        assertEquals(expected, real);
    }

    /**
     * Test of getLast method, of class Circular.
     */
    @Test
    public void testGetLast() {
        test.addLast("E");
        test.addLast("F");
        Object real = test.getLast();
        Object expected = "F";
        assertEquals(expected,real);
    }
    
}
