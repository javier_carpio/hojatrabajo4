/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cooli
 */
public class SimpleTest {
    
    Simple test;
    
    public SimpleTest() {
        test=new Simple();
    }

    /**
     * Test of size method, of class Simple.
     */
    @Test
    public void testSize() {
        int real = test.size();
        int expected =0 ;
        assertEquals(expected,real);
    }

    /**
     * Test of addLast method, of class Simple.
     */
    @Test
    public void testAddLast() {
        test.addLast(1);
        test.addLast(2);
        Object expected = 2;
        Object real = test.getLast();
        assertEquals(expected,real);
    }

    /**
     * Test of getLast method, of class Simple.
     */
    @Test
    public void testGetLast() {
        test.addLast("A");
        test.addLast("B");
        Object expected = "B";
        Object real = test.getLast();
        assertEquals(expected,real);
    }

    /**
     * Test of removeLast method, of class Simple.
     */
    @Test
    public void testRemoveLast() {
        test.addLast(1);
        test.removeLast();
        int expected = 0;
        int real = test.size();
        assertEquals(expected,real);
    }
    
}
