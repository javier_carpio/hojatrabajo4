/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cooli
 */
public class DobleTest {
    
    Doble test;
    
    public DobleTest() {
        test= new Doble();
    }

    /**
     * Test of addLast method, of class Doble.
     */
    @Test
    public void testAddLast() {
        test.addLast(1);
        test.addLast(2);
        Object expected = 2;
        Object real = test.getLast();
        assertEquals(expected,real);
    }

    /**
     * Test of removeLast method, of class Doble.
     */
    @Test
    public void testRemoveLast() {
        test.addLast(1);
        test.addLast(2);
        test.removeLast();
        Object expected = 1;
        Object real = test.getLast();
        assertEquals(expected,real);
    }

    /**
     * Test of size method, of class Doble.
     */
    @Test
    public void testSize() {
        int real = test.size();
        int expected = 0;
        assertEquals(expected,real);
    }

    /**
     * Test of empty method, of class Doble.
     */
    @Test
    public void testEmpty() {
        boolean real = test.empty();
        boolean expected = true;
        assertEquals(expected,real);
    }

    /**
     * Test of push method, of class Doble.
     */
    @Test
    public void testPush() {
        test.push("1");
        boolean real = test.isEmpty();
        boolean expected = false;
        assertEquals(expected,real);
        
    }

    /**
     * Test of pop method, of class Doble.
     */
    @Test
    public void testPop() {
        test.addLast(2);
        Object expected = 2;
        Object real = test.pop();
        assertEquals(expected,real);
        boolean real2=test.isEmpty();
        boolean exp2=true;
        assertEquals(exp2,real2);
    }

    /**
     * Test of peek method, of class Doble.
     */
    @Test
    public void testPeek() {
        test.addLast(1);
        test.addLast(2);
        Object expected = 2;
        Object real = test.peek();
        assertEquals(expected,real);
    }

    /**
     * Test of getLast method, of class Doble.
     */
    @Test
    public void testGetLast() {
        test.addLast(1);
        test.addLast(2);
        Object expected = 2;
        Object real = test.getLast();
        assertEquals(expected,real);
    }
    
}
