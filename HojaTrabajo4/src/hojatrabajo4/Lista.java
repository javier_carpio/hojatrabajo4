/** Lista.java
 * Clase abstracta de listas, implementación de Pila tipo Lista.
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */


public interface Lista<E> extends Stack<E>{
    
    /**
     * Devuelve el tamaño de la lista
     * @return tamaño
     */
   @Override
   public int size();
   // post: returns number of elements in list
   
   /**
    * Verifica si está vacío 
    * @return boolean de vacío o no
    */
   public boolean isEmpty();
   // post: returns true iff list has no elements

   /**
    * Agrega a la última referencia
    * @param value valor a agregar
    */
   public void addLast(E value);
   // post: value is added to end of list

   /**
     * Muestra la última referencia
     * @return valor del último elemento
     */
   public E getLast();
   // pre: list is not empty
   // post: returns last value in list

   /**
     * Elimina la última referencia
     * @return valor del último elemento
     */
   public E removeLast();
   // pre: list is not empty
   // post: removes last value from list

}
