/** StackArrayList.java
 * Implementación de Stack en un ArrayList
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */


import java.util.ArrayList;

/**
 *
 * @author javie
 */
public class StackArrayList <E> implements Stack<E>{
    //Atributos
    protected ArrayList<E> datos;
    
    /**
     * Constructor
     */
    public StackArrayList()
    // post: constructs a new, empty stack
    {
        datos = new ArrayList<>();
    }

    /**
     * Agrega el elemento a la lista
     * @param item elemento a agregar
     */
    @Override
    public void push(E item)
    // post: the value is added to the stack
    {
        datos.add(item);
    }

    /**
     * Elimina el último elemento
     * @return método para eliminar el último elemento
     */
    @Override
    public E pop()
    // pre: stack is not empty
    // post: most recently pushed item is removed and returned
    {
        return datos.remove(size()-1);
    }

    /**
     * Muestra el último elemento agregado a la lista
     * @return último elemento
     */
    @Override
    public E peek()
    // pre: stack is not empty
    // post: top value (next to be popped) is returned  
    {
        return datos.get(size() - 1);
    }

    /**
     * Evalua si está vacío
     * @return boolean de is empty
     */
    @Override
    public boolean empty()
    // post: returns true if and only if the stack is empty
    {
        return size() == 0;
    }

    /**
     * Retorna el tamaño del array
     * @return size
     */
    @Override
    public int size()
    // post: returns the number of elements in the stack
    {
        return datos.size();
    }
    
}
