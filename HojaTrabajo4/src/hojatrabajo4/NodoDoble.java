/** NodoDoble.java
 * Dirige a la cabeza y cola
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */

/**
 *
 * @author javie
 */
public class NodoDoble<E> {
    //Atributos
    protected E data;
    protected NodoDoble<E> nextElement;
    protected NodoDoble<E> previousElement;

    /**
     * Constructor
     * @param v elemento
     * @param next siguiente
     * @param previous anterior
     */
    public NodoDoble(E v, NodoDoble<E> next, NodoDoble<E> previous){
        data = v;
        nextElement = next;
        if (nextElement != null)
            nextElement.previousElement = this;
        previousElement = previous;
        if (previousElement != null)
            previousElement.nextElement = this;
    }

    /**
     * Construye un solo elemento
     * @param v objeto E
     */
    public NodoDoble(E v)
    // post: constructs a single element
    {
        this(v,null,null);
    }
    
    /**
     * Busca el siguiente elemento
     * @return nextElement
     */
    public NodoDoble<E> next()
    // pre: list is not empty
    // post: next element on list
    {
        return nextElement;
    }
    
    /**
     * Coloca el siguiente elemento
     * @param next valor de NodoDoble 
     */
    public void setNext(NodoDoble<E> next)
    // pre: list is not empty
    // post: sets value to the next element on list
    {
        nextElement = next;
    }
    
    /**
     * Retorna el valor de un elemento
     * @return data
     */
    public E value()
    // pre: list is not empty
    // post: returns value 
    {
        return data;
    }
    
    /**
     * Indica el valor de un elemento
     * @param value valor
     */
    public void setValue(E value)
    // pre: list is not empty
    // post: sets value 
    {
        data = value;
    }
    
    /**
     * Asigna un valor al elemento anterior
     * @param previous valor de NodoDoble
     */
    public void setPrevious(NodoDoble<E> previous)
    // pre: list is not empty
    // post: sets value to previous item
    {
        previousElement = previous;
    }
    
    /**
     * Retorna el elemento anterior
     * @return previousElement
     */
    public NodoDoble<E> previous()
    // pre: list is not empty
    // post: returns value from previous item
    {
        return previousElement;
    }
}
