/** Circular.java
 * Lista de implementación circular.
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */


import java.util.Iterator;

/**
 *
 * @author javie
 */
public class Circular<E> extends AbstractList<E>{
    protected Nodo<E> tail; 
    protected int count;

    /**
     * Constructor
     */
    public Circular()
    // pre: constructs a new circular list
    {
       tail = null;
       count = 0;
    }
    
    /**
     * Añade el objeto E al inicio de la lista
     * @param value valor a añadir
     */
    public void addFirst(E value) 
    // pre: value non-null
    // post: adds element to head of list
    {
        Nodo<E> temp = new Nodo<>(value);
        if (tail == null) { // first value added
            tail = temp;
            tail.setNext(tail);
        } else { // element exists in list
            temp.setNext(tail.next());
            tail.setNext(temp);
        }
        count++;
    }

    /**
     * Anade el objeto E al final de la lista
     * @param value valor a anadir
     */
    @Override
    public void addLast(E value)
    // pre: value non-null
    // post: adds element to tail of list
    {
        // new entry:
        addFirst(value);
        tail = tail.next();
    }
    
    /**
     * Remueve el ultimo elemento de la lista
     * @return el valor eliminado
     */
    @Override
    public E removeLast() 
    // pre: !isEmpty()
    // post: returns and removes value from tail of list
    {
        Nodo<E> finger = tail;
        while (finger.next() != tail) {
            finger = finger.next();
        }
        // finger now points to second-to-last value
        Nodo<E> temp = tail;
        if (finger == tail)
        {
            tail = null;
        } else {
            finger.setNext(tail.next());
            tail = finger;
        }
        count--;
        return temp.value();
    }
    
    /**
     * Agrega el elemento a la lista
     * @param item elemento a agregar
     */
     @Override
    public void push(E item) 
    // pre: value non-null
    // post: adds element to head of list
    {
        addLast(item);
    }

    /**
     * Elimina el último elemento
     * @return método para eliminar el último elemento
     */
    @Override
    public E pop() 
    // pre: !isEmpty()
    // post: returns and removes value from tail of list
    {
        return removeLast();
    }

    /**
     * Muestra el último elemento agregado a la lista
     * @return último elemento
     */
    @Override
    public E peek() 
    // pre: !isEmpty()
    // post: returns value from tail of list
    {
        return getLast();
    }
   
    /**
     * Indica el tamaño del nodo
     * @return count el tamaño
     */
    @Override
    public int size()
    // post: returns size of list
    {
        return count;
    }

    /**
     * Muestra el último elemento en la lista
     * @return valor del último elemento
     */
    @Override
    public E getLast() 
    // pre: !isEmpty()
    // post: returns value from tail of list
    {
        return tail.value();
    }

    
}
