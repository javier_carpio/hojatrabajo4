/** Calculadora.java
 * Realización de los cálculos postfix
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */

import static java.lang.Double.NaN;


public class Calculadora {
    //Atributos
    private static Calculadora instance;
    private Stack<Double> stack;
    
    /**
     * Constructor
     * @param tipo stack a evaluar
     */
    private Calculadora(Stack tipo){
        stack = tipo;
    }
    
    /**
     * Implementación de Singleton
     * @param tipo Stack
     * @return instancia de singleton
     */
    public static Calculadora getInstance(Stack tipo){
        if(instance == null){
            instance = new Calculadora(tipo);
        }else{
            System.out.println("Sorry bro, no puedo");
        }
        return instance;
    }
    
    /**
     * Realiza la operación matemática
     * @param expresion String a ingresar
     * @return double de la respuesta
     */
    public double operar(String expresion) {
        boolean permiso = false;
        String operacion = "";
        
        for(int a = 0; a < expresion.length(); a++){
            if(permiso == false){
                operacion = expresion.substring(4, 5);
                permiso = true;
                
                stack.push(Double.parseDouble(expresion.substring(0, 1)));
                expresion = expresion.substring(a + 2);
                stack.push(Double.parseDouble(expresion.substring(0, 1)));
                expresion = expresion.substring(3);
                
                elegirOperacion(operacion);
                
            }else if(permiso == true){
                String dato = expresion.substring(a, a + 1);
                elegirOperacion(dato);
            }
        }
        
        return stack.peek();
    }
    
    /**
     * Realiza la operación dependiendo del signo
     * @param dato signo de operación
     */
    private void elegirOperacion(String dato){
        switch (dato) {
            case "+":
                stack.push(Suma(stack.pop(), stack.pop()));
                break;
            case "-":
                stack.push(Resta(stack.pop(), stack.pop()));
                break;
            case "*":
                stack.push(Multiplicacion(stack.pop(), stack.pop()));
                break;
            case "/":
                stack.push(Division(stack.pop(), stack.pop()));
                break;
            case " ":
                break;
            default:
                stack.push(Double.parseDouble(dato));
                break;
        }
    }
    
    /**
     * Realiza la suma
     * @param num1 numero 1
     * @param num2 numero 2
     * @return suma de num1 + num2
     */
    private double Suma(double num1, double num2){
        return (num1 + num2);
    }
    
    /**
     * Realiza la resta
     * @param num1 numero 1
     * @param num2 numero 2
     * @return resta de num1-num2
     */
    private double Resta(double num1, double num2){
        return (num1 - num2);
    }
    
    /**
     * Multiplica
     * @param num1 numero 1
     * @param num2 numero 2
     * @return multiplicación num1*num2
     */
    private double Multiplicacion(double num1, double num2){
        return (num1*num2);
    }
    
    /**
     * Divide
     * @param num1 numero 1
     * @param num2 numero 2
     * @return num1/num2, NaN si num2 es 0
     */
    private double Division(double num1, double num2){
        double var;
        try{
            var = num1/num2;
        }catch(Exception e){
            var = NaN;
        }
        
        return (var);
    }
    
    
    
    
}
