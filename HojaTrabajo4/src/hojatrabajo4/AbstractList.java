/** AbstractList.java
 * Abstract Class de tipo Lista
 * Javier Carpio 17077
 * Andrea Argüello 17801
 * 14/02/2017
 */
public abstract class AbstractList<E> implements Lista<E>{
    /**
     * Constructor vacío
     */
    public AbstractList()
    // post: nada
    {}
    
    /**
     * Evalua si está vacío o no
     * @return si es 0
     */
    @Override
    public boolean isEmpty()
    // post: returns true if list has no elements
    {
       return size() == 0;
    }
    
    /**
     * Agrega el elemento a la lista
     * @param item elemento a agregar
     */
    @Override
    public void push(E item) 
    // pre: value non-null
    // post: adds element to tail of list
    {
        addLast(item);
    }

    /**
     * Elimina el último elemento
     * @return método para eliminar el último elemento
     */
    @Override
    public E pop() 
    // pre: !isEmpty()
    // post: returns and removes value from tail of list
    {
        return removeLast();
    }

    /**
     * Muestra el último elemento agregado a la lista
     * @return último elemento
     */
    @Override
    public E peek()
    // pre: !isEmpty()
    // post: returns value from tail of list
    {
        return getLast();
    }

    /**
     * Pone el tamaño como 0
     * @return boolean de is empty
     */
    @Override
    public boolean empty()
    // post: returns true if and only if the stack is empty
    {
        return isEmpty();
    }

    /**
     * Indica el tamaño del nodo
     * @return count el tamaño
     */
    @Override
    public int size()
    // post: returns the number of elements in the stack
    {
        return 0;
    }
    
}
